import React from 'react';
import ReactDOM from 'react-dom';



//Component Son of App

const Header = ({course}) => <h1>{course}</h1>

//Component Son of App

const Content = ({part,exercise}) => <p>{part} : {exercise}</p>

//Component Son of App

const Total = ({exercise}) =><p>Number of exercise : {exercise}</p>

//Component Father

const App = () => {

  const course = 'Half Stack application development'
  const part1 = 'Fundamentals of React'
  const exercise1 = 10
  const part2 = 'Using props to pass data'
  const exercise2 = 7
  const part3 = 'State of a component'
  const exercise3 = 14


  return(
    <>
      <Header course={course}/>
      <Content  part={part1} exercise={exercise1}/>
      <Content  part={part2} exercise={exercise2}/>
      <Content  part={part3} exercise={exercise3}/>
      <Total exercise={exercise1 + exercise2 + exercise3}/>
    </>

  )
}

ReactDOM.render(<App />, document.getElementById('root'))
